package com.kyriba.log;

import com.kyriba.log.model.FilterParameters;
import com.kyriba.log.model.Group;
import com.kyriba.log.model.GroupingParameters;
import com.kyriba.log.model.LogRecord;
import com.kyriba.log.service.GroupCreator;
import com.kyriba.log.service.LogRecordFilter;
import com.kyriba.log.service.util.DateFilepathParser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class LogCreator {
    private final String[] users = {"Nikita", "Alex", "Peter", "Bob", "Alice", "Bill", "Gordon", "Liza", "Jon", "Lily"};
    private final String[] messages = {
            "ERROR: Database is unavailable",
            "WARNING: Time limit for database query is exceed",
            "INFO: Connected to the database",
            "ERROR: Entity not found by ID",
            "WARNING: New entity was created but wasn't saved",
            "INFO: 10 entities were found",
            "ERROR: Process was interrupted",
            "WARNING: Possible leak of memory in a process",
            "INFO: New process was created"
    };
    private final TemporalUnit intervalBetweenLogRecords = ChronoUnit.MINUTES;
    private final Random random = new Random();
    private final FilterParameters filterParameters;
    private final GroupingParameters groupingParameters;
    private final LogRecordFilter logRecordFilter;
    private final GroupCreator groupCreator;
    private final Map<Group, Long> groupingResult;

    public LogCreator(FilterParameters filterParameters, GroupingParameters groupingParameters,
                      LogRecordFilter logRecordFilter, GroupCreator groupCreator,
                      Map<Group, Long> groupingResult) {
        this.filterParameters = filterParameters;
        this.groupingParameters = groupingParameters;
        this.logRecordFilter = logRecordFilter;
        this.groupCreator = groupCreator;
        this.groupingResult = groupingResult;
    }

    public long createLogs(Path logFolderPath, long logFilesNumber) throws IOException {
        long recordsApprovedByFilter = 0;
        for (long i = 0; i < logFilesNumber; i++) {
            LocalDate logDate = LocalDate.now().minus(logFilesNumber - i, ChronoUnit.DAYS);
            recordsApprovedByFilter += createLogFile(logFolderPath, logDate);
        }
        return recordsApprovedByFilter;
    }

    private long createLogFile(Path logFolderPath, LocalDate logDate) throws IOException {
        long recordsApprovedByFilter = 0;
        Path logFilePath = DateFilepathParser.parse(logFolderPath, logDate);
        List<String> logLines = new LinkedList<>();
        LocalTime time = LocalTime.MIN;
        do {
            LogRecord logRecord = createLogRecord(time);
            if (logRecordFilter.isRecordMatches(logRecord, filterParameters)) {
                recordsApprovedByFilter++;
                Group group = groupCreator.create(logDate, logRecord, groupingParameters);
                groupingResult.merge(group, 1L, Long::sum);
            }
            logLines.add(logRecord.toString());
            time = time.plus(1, intervalBetweenLogRecords);
        } while (!time.equals(LocalTime.MIN));
        Files.write(logFilePath, logLines);
        return recordsApprovedByFilter;
    }

    private LogRecord createLogRecord(LocalTime time) {
        String username = users[random.nextInt(users.length)];
        String message = messages[random.nextInt(messages.length)];
        return new LogRecord(time, username, message);
    }

    public long getNumberOfLogRecordsPerFile() {
        return ChronoUnit.DAYS.getDuration().dividedBy(intervalBetweenLogRecords.getDuration());
    }
}
