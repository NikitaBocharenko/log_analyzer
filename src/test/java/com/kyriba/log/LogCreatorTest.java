package com.kyriba.log;

import com.kyriba.log.model.FilterParameters;
import com.kyriba.log.model.Group;
import com.kyriba.log.model.GroupingParameters;
import com.kyriba.log.service.GroupCreator;
import com.kyriba.log.service.GroupCreatorImpl;
import com.kyriba.log.service.LogRecordFilter;
import com.kyriba.log.service.LogRecordFilterImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class LogCreatorTest {
    private static final String LOG_DIRECTORY = "\\logs";
    private static final Long LOG_FILES_NUMBER = 1000L;

    private LogCreator logCreator;
    private final Path logDirectoryPath = Paths.get(LOG_DIRECTORY);
    private final Map<Group, Long> groupingResult = new HashMap<>();

    @Before
    public void setUp() throws IOException {
        FilterParameters filterParameters = new FilterParameters(null, null, null, null);
        GroupingParameters groupingParameters = new GroupingParameters(false, null);
        LogRecordFilter logRecordFilter = new LogRecordFilterImpl();
        GroupCreator groupCreator = new GroupCreatorImpl();
        logCreator = new LogCreator(filterParameters, groupingParameters, logRecordFilter, groupCreator, groupingResult);
        Files.createDirectory(logDirectoryPath);
    }

    @Test
    public void shouldCreateLogFiles() throws IOException {
        long recordsApprovedByFilterCount = logCreator.createLogs(logDirectoryPath, LOG_FILES_NUMBER);
        Long expectedLogRecordsNumberPerFile = logCreator.getNumberOfLogRecordsPerFile();
        assertEquals(LOG_FILES_NUMBER * expectedLogRecordsNumberPerFile, recordsApprovedByFilterCount);
        assertEquals(LOG_FILES_NUMBER * expectedLogRecordsNumberPerFile, groupingResult.get(new Group(null, null)).intValue());
        Long actualFilesNumber = getCreatedFilesNumber();
        assertEquals(LOG_FILES_NUMBER, actualFilesNumber);
        try (Stream<Path> walk = Files.walk(logDirectoryPath)) {
            walk
                    .filter(Files::isRegularFile)
                    .map(this::countAllLinesByPath)
                    .forEach(actualRecordsNumber -> assertEquals(expectedLogRecordsNumberPerFile, actualRecordsNumber));
        }
    }

    @After
    public void cleanUp() throws IOException {
        try (Stream<Path> walk = Files.walk(logDirectoryPath)) {
            walk
                    .sorted(Comparator.reverseOrder())
                    .forEach(this::deleteByPath);
        }
    }

    private void deleteByPath(Path path) {
        try {
            Files.delete(path);
        } catch (IOException e) {
            throw new IllegalArgumentException("Cannot delete by path: " + path + " " + e.getMessage());
        }
    }

    private Long getCreatedFilesNumber() throws IOException {
        return Files.find(
                logDirectoryPath,
                1,
                (path, attributes) -> attributes.isRegularFile()
        ).count();
    }

    private long countAllLinesByPath(Path path) {
        try {
            return Files.readAllLines(path).size();
        } catch (IOException e) {
            throw new IllegalArgumentException("Cannot count lines in file: " + path + " " + e.getMessage());
        }
    }
}
