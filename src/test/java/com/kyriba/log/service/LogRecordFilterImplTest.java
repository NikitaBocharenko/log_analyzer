package com.kyriba.log.service;

import com.kyriba.log.model.FilterParameters;
import com.kyriba.log.model.LogRecord;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.time.LocalTime;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class LogRecordFilterImplTest {
    private final LogRecordFilter logRecordFilter = new LogRecordFilterImpl();
    private FilterParameters filterParameters;

    @Before
    public void setUp() {
        filterParameters = new FilterParameters();
        filterParameters.setUsername("Nikita");
        filterParameters.setTimeFrom(LocalTime.of(10, 0));
        filterParameters.setTimeTo(LocalTime.of(15, 0));
        filterParameters.setMessageRegex("TEST.*");
    }

    @Test
    public void shouldApproveLogRecord() {
        LogRecord record = new LogRecord(LocalTime.of(11 ,0), "Nikita", "TEST");
        boolean result = logRecordFilter.isRecordMatches(record, filterParameters);
        assertTrue(result);
    }

    @Test
    public void shouldRejectRecordByUsername() {
        LogRecord record = new LogRecord(LocalTime.of(11 ,0), "Alex", "TEST");
        boolean result = logRecordFilter.isRecordMatches(record, filterParameters);
        assertFalse(result);
    }

    @Test
    public void shouldRejectRecordByTimeFrom() {
        LogRecord record = new LogRecord(LocalTime.of(9 ,0), "Nikita", "TEST");
        boolean result = logRecordFilter.isRecordMatches(record, filterParameters);
        assertFalse(result);
    }

    @Test
    public void shouldRejectRecordByTimeTo() {
        LogRecord record = new LogRecord(LocalTime.of(16 ,0), "Nikita", "TEST");
        boolean result = logRecordFilter.isRecordMatches(record, filterParameters);
        assertFalse(result);
    }

    @Test
    public void shouldRejectRecordByMessageRegex() {
        LogRecord record = new LogRecord(LocalTime.of(11 ,0), "Nikita", "some message");
        boolean result = logRecordFilter.isRecordMatches(record, filterParameters);
        assertFalse(result);
    }
}
