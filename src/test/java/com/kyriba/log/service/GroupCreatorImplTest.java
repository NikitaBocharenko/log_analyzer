package com.kyriba.log.service;

import com.kyriba.log.model.Group;
import com.kyriba.log.model.GroupingParameters;
import com.kyriba.log.model.LogRecord;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(JUnit4.class)
public class GroupCreatorImplTest {
    private final GroupCreator groupCreator = new GroupCreatorImpl();
    private final LocalDate date = LocalDate.of(2020, 5, 6);
    private final LogRecord record = new LogRecord(LocalTime.of(11, 20), "Nikita", "test message");

    @Test
    public void shouldCreateGroupWithUsername() {
        GroupingParameters parameters = new GroupingParameters(true, null);
        Group group = groupCreator.create(date, record, parameters);
        assertEquals(record.getUsername(), group.getUsername());
        assertNull(group.getDateTime());
    }

    @Test
    public void shouldCreateGroupWithDayTruncatedDateTime() {
        GroupingParameters parameters = new GroupingParameters(false, ChronoUnit.DAYS);
        Group group = groupCreator.create(date, record, parameters);
        LocalDateTime expected = LocalDateTime.of(date, record.getTime()).truncatedTo(ChronoUnit.DAYS);
        assertEquals(expected, group.getDateTime());
        assertNull(group.getUsername());
    }

    @Test
    public void shouldCreateGroupWithUsernameAndMonthTruncatedDateTime() {
        GroupingParameters parameters = new GroupingParameters(true, ChronoUnit.MONTHS);
        Group group = groupCreator.create(date, record, parameters);
        LocalDateTime expected = LocalDateTime.of(date, record.getTime()).withDayOfMonth(1).truncatedTo(ChronoUnit.DAYS);
        assertEquals(expected, group.getDateTime());
        assertEquals(record.getUsername(), group.getUsername());
    }

    @Test
    public void shouldCreateGroupWithUsernameAndYearTruncatedDateTime() {
        GroupingParameters parameters = new GroupingParameters(true, ChronoUnit.YEARS);
        Group group = groupCreator.create(date, record, parameters);
        LocalDateTime expected = LocalDateTime.of(date, record.getTime()).withMonth(1).withDayOfMonth(1).truncatedTo(ChronoUnit.DAYS);
        assertEquals(expected, group.getDateTime());
        assertEquals(record.getUsername(), group.getUsername());
    }

    @Test
    public void shouldCreateGroupWithUsernameAndMinuteTruncatedDateTime() {
        GroupingParameters parameters = new GroupingParameters(true, ChronoUnit.MINUTES);
        Group group = groupCreator.create(date, record, parameters);
        LocalDateTime expected = LocalDateTime.of(date, record.getTime()).truncatedTo(ChronoUnit.MINUTES);
        assertEquals(expected, group.getDateTime());
        assertEquals(record.getUsername(), group.getUsername());
    }
}
