package com.kyriba.log.service;

import com.kyriba.log.LogCreator;
import com.kyriba.log.model.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class LogAnalyzerCallableTest {
    private LogAnalyzerCallable logAnalyzerCallable;
    private AnalysisParameters analysisParameters;
    private long expectedRecordsApprovedByFilterCount;

    private final Path logDirectoryPath = Paths.get(LOG_DIRECTORY);
    private final Set<Path> analyzedFiles = new HashSet<>();
    private final Map<Group, Long> expectedGroupingResult = new HashMap<>();

    private static final String LOG_DIRECTORY = "\\logs";
    private static final String OUTPUT_FILE_NAME = "!logAnalysisResult.txt";
    private static final Integer LOG_FILES_NUMBER = 1000;

    @Before
    public void setUp() throws IOException {
        LogRecordFilter logRecordFilter = new LogRecordFilterImpl();
        OutputFileWriter outputFileWriter = new OutputFileWriterImpl();
        GroupCreator groupCreator = new GroupCreatorImpl();
        prepareAnalysisParameters();
        logAnalyzerCallable = new LogAnalyzerCallable(analysisParameters, analyzedFiles, logRecordFilter, groupCreator, outputFileWriter);
        Files.createDirectory(logDirectoryPath);
        LogCreator logCreator = new LogCreator(analysisParameters.getFilterParameters(), analysisParameters.getGroupingParameters(), logRecordFilter,
                groupCreator, expectedGroupingResult);
        expectedRecordsApprovedByFilterCount = logCreator.createLogs(logDirectoryPath, LOG_FILES_NUMBER);
    }

    @Test
    public void shouldAnalyzeLogs() throws IOException {
        Map<Group, Long> actualGroupingResult = logAnalyzerCallable.call();
        assertEquals(LOG_FILES_NUMBER.intValue(), analyzedFiles.size());
        assertEquals(expectedGroupingResult, actualGroupingResult);
        Path resultFilePath = analysisParameters.getOtherParameters().getOutputFilePath();
        long actualRecordsApprovedByFilterCount;
        try (Stream<String> resultFileLinesStream = Files.lines(resultFilePath)) {
            actualRecordsApprovedByFilterCount = resultFileLinesStream.count();
        }
        assertEquals(expectedRecordsApprovedByFilterCount, actualRecordsApprovedByFilterCount);
    }

    @After
    public void cleanUp() throws IOException {
        try (Stream<Path> walk = Files.walk(logDirectoryPath)) {
            walk
                    .sorted(Comparator.reverseOrder())
                    .forEach(this::deleteByPath);
        }
    }

    private void deleteByPath(Path path) {
        try {
            Files.delete(path);
        } catch (IOException e) {
            throw new IllegalArgumentException("Cannot delete by path: " + path + " " + e.getMessage());
        }
    }

    private void prepareAnalysisParameters() {
        FilterParameters filterParameters = new FilterParameters();
        filterParameters.setUsername("Nikita");
        filterParameters.setMessageRegex(".*ERROR.*");
        filterParameters.setTimeFrom(LocalTime.of(10, 0));
        filterParameters.setTimeTo(LocalTime.of(16, 0));
        GroupingParameters groupingParameters = new GroupingParameters();
        groupingParameters.setGroupByUsername(true);
        groupingParameters.setTimeUnitGroup(ChronoUnit.DAYS);
        OtherParameters otherParameters = new OtherParameters();
        otherParameters.setLogFolderPath(logDirectoryPath);
        Path outputFilePath = logDirectoryPath.resolve(OUTPUT_FILE_NAME);
        otherParameters.setOutputFilePath(outputFilePath);
        analysisParameters = new AnalysisParameters(filterParameters, groupingParameters, otherParameters);
    }
}
