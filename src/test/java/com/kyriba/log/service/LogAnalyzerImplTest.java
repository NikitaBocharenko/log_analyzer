package com.kyriba.log.service;

import com.kyriba.log.LogCreator;
import com.kyriba.log.model.*;
import com.kyriba.log.validation.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class LogAnalyzerImplTest {
    private LogAnalyzer logAnalyzer;
    private AnalysisParameters analysisParameters;
    private AnalysisParametersValidator analysisParametersValidator;
    private final Path logDirectoryPath = Paths.get(LOG_DIRECTORY);
    private final Map<Group, Long> expectedGroupingResult = new HashMap<>();

    private static final String LOG_DIRECTORY = "\\logs";
    private static final String OUTPUT_FILE_NAME = "!logAnalysisResult.txt";
    private static final Integer THREAD_COUNT = 10;
    private static final Long LOG_FILES_NUMBER = 2000L;

    @Before
    public void setUp() throws IOException {
        prepareAnalysisParametersValidator();
        LogRecordFilter logRecordFilter = new LogRecordFilterImpl();
        OutputFileWriter outputFileWriter = new OutputFileWriterImpl();
        GroupCreator groupCreator = new GroupCreatorImpl();
        GroupingTableCreator groupingTableCreator = new GroupingTableCreatorImpl();
        logAnalyzer = new LogAnalyzerImpl(analysisParametersValidator, logRecordFilter, groupCreator, groupingTableCreator, outputFileWriter);
        prepareAnalysisParameters();
        Path logDirectoryPath = Paths.get(LOG_DIRECTORY);
        Files.createDirectory(logDirectoryPath);
        LogCreator logCreator = new LogCreator(analysisParameters.getFilterParameters(), analysisParameters.getGroupingParameters(), logRecordFilter, groupCreator, expectedGroupingResult);
        logCreator.createLogs(logDirectoryPath, LOG_FILES_NUMBER);
    }

    @Test
    public void shouldAnalyzeLogs() {
        Map<Group, Long> actualGroupingResult = logAnalyzer.makeAnalysis(analysisParameters);
        assertEquals(expectedGroupingResult, actualGroupingResult);
    }

    private void prepareAnalysisParameters() {
        FilterParameters filterParameters = new FilterParameters();
        filterParameters.setMessageRegex("ERROR.*");
        filterParameters.setTimeFrom(LocalTime.of(8, 0));
        filterParameters.setTimeTo(LocalTime.of(22, 0));
        GroupingParameters groupingParameters = new GroupingParameters();
        groupingParameters.setGroupByUsername(true);
        groupingParameters.setTimeUnitGroup(ChronoUnit.DAYS);
        OtherParameters otherParameters = new OtherParameters();
        otherParameters.setLogFolderPath(logDirectoryPath);
        Path outputFilePath = Paths.get(OUTPUT_FILE_NAME);
        otherParameters.setOutputFilePath(outputFilePath);
        otherParameters.setProcessingThreadNumber(THREAD_COUNT);
        analysisParameters = new AnalysisParameters(filterParameters, groupingParameters, otherParameters);
    }

    private void prepareAnalysisParametersValidator() {
        FilterParametersValidator filterParametersValidator = new FilterParametersValidatorImpl();
        GroupingParametersValidator groupingParametersValidator = new GroupingParametersValidatorImpl();
        OtherParametersValidator otherParametersValidator = new OtherParametersValidatorImpl();
        analysisParametersValidator = new AnalysisParametersValidatorImpl(filterParametersValidator,
                groupingParametersValidator, otherParametersValidator);
    }

    @After
    public void cleanUp() throws IOException {
        try (Stream<Path> walk = Files.walk(logDirectoryPath)) {
            walk
                    .sorted(Comparator.reverseOrder())
                    .forEach(this::deleteByPath);
        }
    }

    private void deleteByPath(Path path) {
        try {
            Files.delete(path);
        } catch (IOException e) {
            throw new IllegalArgumentException("Cannot delete by path: " + path + " " + e.getMessage());
        }
    }

}
