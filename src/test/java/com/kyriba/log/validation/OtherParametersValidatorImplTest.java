package com.kyriba.log.validation;

import com.kyriba.log.model.OtherParameters;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.file.Paths;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class OtherParametersValidatorImplTest {
    private final OtherParametersValidator validator = new OtherParametersValidatorImpl();

    @Test
    public void shouldValidateParameters() {
        OtherParameters parameters = new OtherParameters(Paths.get("testPath"), 1, Paths.get("testPath"));
        boolean result = validator.validate(parameters);
        assertTrue(result);
    }

    @Test
    public void shouldRejectParametersByLogFolderPath() {
        OtherParameters parameters = new OtherParameters(null, 1, Paths.get("testPath"));
        boolean result = validator.validate(parameters);
        assertFalse(result);
    }

    @Test
    public void shouldRejectParametersByOutputFilePath() {
        OtherParameters parameters = new OtherParameters(Paths.get("testPath"), 1, null);
        boolean result = validator.validate(parameters);
        assertFalse(result);
    }
}
