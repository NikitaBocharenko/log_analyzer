package com.kyriba.log.validation;

import com.kyriba.log.model.GroupingParameters;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class GroupingParametersValidatorImplTest {
    private final GroupingParametersValidator validator = new GroupingParametersValidatorImpl();

    @Test
    public void shouldValidateParameters() {
        GroupingParameters parameters = new GroupingParameters(true, null);
        boolean result = validator.validate(parameters);
        assertTrue(result);
    }

    @Test
    public void shouldRejectParameters() {
        GroupingParameters parameters = new GroupingParameters(false, null);
        boolean result = validator.validate(parameters);
        assertFalse(result);
    }
}
