package com.kyriba.log.validation;

import com.kyriba.log.model.FilterParameters;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class FilterParametersValidatorImplTest {
    private final FilterParametersValidator validator = new FilterParametersValidatorImpl();

    @Test
    public void shouldValidateFilterParameters() {
        FilterParameters parameters = new FilterParameters("Username", null, null, null);
        boolean result = validator.validate(parameters);
        assertTrue(result);
    }

    @Test
    public void shouldRejectFilterParameters() {
        FilterParameters parameters = new FilterParameters(null, null, null, null);
        boolean result = validator.validate(parameters);
        assertFalse(result);
    }
}
