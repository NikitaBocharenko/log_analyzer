package com.kyriba.log.validation;

import com.kyriba.log.model.GroupingParameters;

public interface GroupingParametersValidator {
    boolean validate(GroupingParameters parameters);
}
