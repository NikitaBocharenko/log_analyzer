package com.kyriba.log.validation;

import com.kyriba.log.model.OtherParameters;

public interface OtherParametersValidator {
    boolean validate(OtherParameters parameters);
}
