package com.kyriba.log.validation;

import com.kyriba.log.model.FilterParameters;

public interface FilterParametersValidator {
    boolean validate(FilterParameters parameters);
}
