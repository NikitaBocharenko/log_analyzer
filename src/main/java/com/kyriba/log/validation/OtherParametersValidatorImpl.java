package com.kyriba.log.validation;

import com.kyriba.log.model.OtherParameters;

public class OtherParametersValidatorImpl implements OtherParametersValidator {
    @Override
    public boolean validate(OtherParameters parameters) {
        return parameters.getLogFolderPath() != null && parameters.getOutputFilePath() != null;
    }
}
