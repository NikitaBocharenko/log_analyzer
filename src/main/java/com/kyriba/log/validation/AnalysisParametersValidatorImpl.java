package com.kyriba.log.validation;

import com.kyriba.log.model.AnalysisParameters;
import com.kyriba.log.model.FilterParameters;
import com.kyriba.log.model.GroupingParameters;
import com.kyriba.log.model.OtherParameters;

public class AnalysisParametersValidatorImpl implements AnalysisParametersValidator {
    private final FilterParametersValidator filterParametersValidator;
    private final GroupingParametersValidator groupingParametersValidator;
    private final OtherParametersValidator otherParametersValidator;

    public AnalysisParametersValidatorImpl(FilterParametersValidator filterParametersValidator,
                                           GroupingParametersValidator groupingParametersValidator,
                                           OtherParametersValidator otherParametersValidator) {
        this.filterParametersValidator = filterParametersValidator;
        this.groupingParametersValidator = groupingParametersValidator;
        this.otherParametersValidator = otherParametersValidator;
    }

    @Override
    public boolean validate(AnalysisParameters parameters) {
        FilterParameters filterParameters = parameters.getFilterParameters();
        GroupingParameters groupingParameters = parameters.getGroupingParameters();
        OtherParameters otherParameters = parameters.getOtherParameters();
        return filterParametersValidator.validate(filterParameters) &&
                groupingParametersValidator.validate(groupingParameters) &&
                otherParametersValidator.validate(otherParameters);
    }
}
