package com.kyriba.log.validation;

import com.kyriba.log.model.AnalysisParameters;

public interface AnalysisParametersValidator {
    boolean validate(AnalysisParameters parameters);
}
