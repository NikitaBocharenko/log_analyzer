package com.kyriba.log.validation;

import com.kyriba.log.model.GroupingParameters;

import java.time.temporal.TemporalUnit;

public class GroupingParametersValidatorImpl implements GroupingParametersValidator {
    @Override
    public boolean validate(GroupingParameters parameters) {
        boolean isGroupByUsername = parameters.isGroupByUsername();
        TemporalUnit groupingTimeUnit = parameters.getTimeUnitGroup();
        return isGroupByUsername || groupingTimeUnit != null;
    }
}
