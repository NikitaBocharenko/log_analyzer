package com.kyriba.log.validation;

import com.kyriba.log.model.FilterParameters;

import java.time.LocalTime;

public class FilterParametersValidatorImpl implements FilterParametersValidator {
    @Override
    public boolean validate(FilterParameters parameters) {
        String username = parameters.getUsername();
        LocalTime timeFrom = parameters.getTimeFrom();
        LocalTime timeTo = parameters.getTimeTo();
        String messageRegex = parameters.getMessageRegex();
        return username != null || timeFrom != null || timeTo != null || messageRegex != null;
    }
}
