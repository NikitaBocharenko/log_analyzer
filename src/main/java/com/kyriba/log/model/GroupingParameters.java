package com.kyriba.log.model;

import java.time.temporal.TemporalUnit;

public class GroupingParameters {
    private boolean isGroupByUsername;
    private TemporalUnit timeUnitGroup;

    public GroupingParameters() {
    }

    public GroupingParameters(boolean isGroupByUsername, TemporalUnit timeUnitGroup) {
        this.isGroupByUsername = isGroupByUsername;
        this.timeUnitGroup = timeUnitGroup;
    }

    public boolean isGroupByUsername() {
        return isGroupByUsername;
    }

    public TemporalUnit getTimeUnitGroup() {
        return timeUnitGroup;
    }

    public void setGroupByUsername(boolean groupByUsername) {
        isGroupByUsername = groupByUsername;
    }

    public void setTimeUnitGroup(TemporalUnit timeUnitGroup) {
        this.timeUnitGroup = timeUnitGroup;
    }
}
