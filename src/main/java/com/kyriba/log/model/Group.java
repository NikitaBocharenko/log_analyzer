package com.kyriba.log.model;

import java.time.LocalDateTime;
import java.util.Objects;

public class Group {
    private final String username;
    private final LocalDateTime dateTime;

    public Group(String username, LocalDateTime dateTime) {
        this.username = username;
        this.dateTime = dateTime;
    }

    public String getUsername() {
        return username;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(username, group.username) &&
                Objects.equals(dateTime, group.dateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, dateTime);
    }
}
