package com.kyriba.log.model;

import java.time.LocalTime;

public class FilterParameters {
    private String username;
    private LocalTime timeFrom;
    private LocalTime timeTo;
    private String messageRegex;

    public FilterParameters(String username, LocalTime timeFrom, LocalTime timeTo, String messageRegex) {
        this.username = username;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
        this.messageRegex = messageRegex;
    }

    public FilterParameters() {
    }

    public String getUsername() {
        return username;
    }

    public LocalTime getTimeFrom() {
        return timeFrom;
    }

    public LocalTime getTimeTo() {
        return timeTo;
    }

    public String getMessageRegex() {
        return messageRegex;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setTimeFrom(LocalTime timeFrom) {
        this.timeFrom = timeFrom;
    }

    public void setTimeTo(LocalTime timeTo) {
        this.timeTo = timeTo;
    }

    public void setMessageRegex(String messageRegex) {
        this.messageRegex = messageRegex;
    }
}
