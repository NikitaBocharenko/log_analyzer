package com.kyriba.log.model;

import java.nio.file.Path;

public class OtherParameters {
    private Path logFolderPath;
    private int processingThreadNumber = 1;
    private Path outputFilePath;

    public OtherParameters(Path logFolderPath, int processingThreadNumber, Path outputFilePath) {
        this.logFolderPath = logFolderPath;
        this.processingThreadNumber = processingThreadNumber;
        this.outputFilePath = outputFilePath;
    }

    public OtherParameters() {
    }

    public int getProcessingThreadNumber() {
        return processingThreadNumber;
    }

    public Path getOutputFilePath() {
        return outputFilePath;
    }

    public Path getLogFolderPath() {
        return logFolderPath;
    }

    public void setProcessingThreadNumber(int processingThreadNumber) {
        this.processingThreadNumber = processingThreadNumber;
    }

    public void setOutputFilePath(Path outputFilePath) {
        this.outputFilePath = outputFilePath;
    }

    public void setLogFolderPath(Path logFolderPath) {
        this.logFolderPath = logFolderPath;
    }
}
