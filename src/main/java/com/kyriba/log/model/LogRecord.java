package com.kyriba.log.model;

import java.time.LocalTime;

public class LogRecord {
    private final LocalTime time;
    private final String username;
    private final String message;

    public LogRecord(LocalTime time, String username, String message) {
        this.time = time;
        this.username = username;
        this.message = message;
    }

    public LogRecord(String recordString) {
        String timeString = recordString.substring(0, recordString.indexOf(" "));
        this.time = LocalTime.parse(timeString);
        recordString = recordString.substring(recordString.indexOf(" ") + 1);
        this.username = recordString.substring(0, recordString.indexOf(" "));
        this.message = recordString.substring(recordString.indexOf(" ") + 1);
    }

    public LocalTime getTime() {
        return time;
    }

    public String getUsername() {
        return username;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return time.toString() + " " + username + " " + message;
    }
}
