package com.kyriba.log.model;

public class AnalysisParameters {
    private FilterParameters filterParameters;
    private GroupingParameters groupingParameters;
    private OtherParameters otherParameters;

    public AnalysisParameters(FilterParameters filterParameters, GroupingParameters groupingParameters, OtherParameters otherParameters) {
        this.filterParameters = filterParameters;
        this.groupingParameters = groupingParameters;
        this.otherParameters = otherParameters;
    }

    public AnalysisParameters() {
    }

    public FilterParameters getFilterParameters() {
        return filterParameters;
    }

    public GroupingParameters getGroupingParameters() {
        return groupingParameters;
    }

    public OtherParameters getOtherParameters() {
        return otherParameters;
    }

    public void setFilterParameters(FilterParameters filterParameters) {
        this.filterParameters = filterParameters;
    }

    public void setGroupingParameters(GroupingParameters groupingParameters) {
        this.groupingParameters = groupingParameters;
    }

    public void setOtherParameters(OtherParameters otherParameters) {
        this.otherParameters = otherParameters;
    }
}
