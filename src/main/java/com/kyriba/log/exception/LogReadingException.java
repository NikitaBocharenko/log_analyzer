package com.kyriba.log.exception;

public class LogReadingException extends RuntimeException {
    public LogReadingException(String message, Throwable cause) {
        super(message, cause);
    }
}
