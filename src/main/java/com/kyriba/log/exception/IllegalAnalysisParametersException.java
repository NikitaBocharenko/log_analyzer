package com.kyriba.log.exception;

public class IllegalAnalysisParametersException extends RuntimeException {
    public IllegalAnalysisParametersException(String message) {
        super(message);
    }
}
