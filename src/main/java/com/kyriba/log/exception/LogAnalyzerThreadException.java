package com.kyriba.log.exception;

public class LogAnalyzerThreadException extends RuntimeException {
    public LogAnalyzerThreadException(String message, Throwable cause) {
        super(message, cause);
    }
}
