package com.kyriba.log.exception;

public class OutputFileWritingException extends RuntimeException {
    public OutputFileWritingException(String message, Throwable cause) {
        super(message, cause);
    }
}
