package com.kyriba.log.service;

import com.kyriba.log.model.AnalysisParameters;
import com.kyriba.log.model.Group;

import java.util.Map;

public interface LogAnalyzer {
    Map<Group, Long> makeAnalysis(AnalysisParameters parameters);
}
