package com.kyriba.log.service;

import com.kyriba.log.exception.IllegalAnalysisParametersException;
import com.kyriba.log.exception.LogAnalyzerThreadException;
import com.kyriba.log.model.AnalysisParameters;
import com.kyriba.log.model.Group;
import com.kyriba.log.validation.AnalysisParametersValidator;

import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.*;

public class LogAnalyzerImpl implements LogAnalyzer {
    private final AnalysisParametersValidator parametersValidator;
    private final LogRecordFilter logRecordFilter;
    private final GroupCreator groupCreator;
    private final GroupingTableCreator groupingTableCreator;
    private final OutputFileWriter outputFileWriter;

    public LogAnalyzerImpl(AnalysisParametersValidator parametersValidator, LogRecordFilter logRecordFilter,
                           GroupCreator groupCreator, GroupingTableCreator groupingTableCreator, OutputFileWriter outputFileWriter) {
        this.parametersValidator = parametersValidator;
        this.logRecordFilter = logRecordFilter;
        this.groupCreator = groupCreator;
        this.groupingTableCreator = groupingTableCreator;
        this.outputFileWriter = outputFileWriter;
    }

    @Override
    public Map<Group, Long> makeAnalysis(AnalysisParameters parameters) {
        if (!parametersValidator.validate(parameters)) {
            throw new IllegalAnalysisParametersException("Wrong analysis parameters");
        }
        List<Future<Map<Group, Long>>> analyzersResults = runAnalyzers(parameters);
        Map<Group, Long> groupingResult = collectAnalyzersResults(analyzersResults);
        List<String> tableRows = groupingTableCreator.createTableRows(parameters.getGroupingParameters(), groupingResult);
        outputFileWriter.writeLinesToFile(tableRows, parameters.getOtherParameters().getOutputFilePath());
        return groupingResult;
    }

    private List<Future<Map<Group, Long>>> runAnalyzers(AnalysisParameters parameters) {
        final Set<Path> analyzedFiles = ConcurrentHashMap.newKeySet();
        int threadNumber = parameters.getOtherParameters().getProcessingThreadNumber();
        final ExecutorService analysisThreadPool = Executors.newFixedThreadPool(threadNumber);
        List<Future<Map<Group, Long>>> threadWorkResults = new LinkedList<>();
        for (int i = 0; i < threadNumber; i++) {
            LogAnalyzerCallable task = new LogAnalyzerCallable(parameters, analyzedFiles, logRecordFilter, groupCreator, outputFileWriter);
            Future<Map<Group, Long>> future = analysisThreadPool.submit(task);
            threadWorkResults.add(future);
        }
        return threadWorkResults;
    }

    private Map<Group, Long> collectAnalyzersResults(List<Future<Map<Group, Long>>> analyzersResults) {
        final Map<Group, Long> groupingResult = new HashMap<>();
        try {
            for (Future<Map<Group, Long>> future : analyzersResults) {
                Map<Group, Long> taskGroupingResult = future.get();
                taskGroupingResult.forEach((group, aLong) -> groupingResult.merge(group, aLong, Long::sum));
            }
        } catch (InterruptedException | ExecutionException e) {
            throw new LogAnalyzerThreadException("Exception in log analyzer thread work: " + e.getMessage(), e);
        }
        return groupingResult;
    }
}
