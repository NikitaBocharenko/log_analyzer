package com.kyriba.log.service;

import com.kyriba.log.model.FilterParameters;
import com.kyriba.log.model.LogRecord;

public interface LogRecordFilter {
    boolean isRecordMatches(LogRecord record, FilterParameters parameters);
}
