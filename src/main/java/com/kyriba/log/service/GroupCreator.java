package com.kyriba.log.service;

import com.kyriba.log.model.Group;
import com.kyriba.log.model.GroupingParameters;
import com.kyriba.log.model.LogRecord;

import java.time.LocalDate;

public interface GroupCreator {
    Group create(LocalDate logDate, LogRecord logRecord, GroupingParameters groupingParameters);
}
