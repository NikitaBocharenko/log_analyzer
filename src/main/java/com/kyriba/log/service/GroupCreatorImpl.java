package com.kyriba.log.service;

import com.kyriba.log.model.Group;
import com.kyriba.log.model.GroupingParameters;
import com.kyriba.log.model.LogRecord;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;

public class GroupCreatorImpl implements GroupCreator {

    @Override
    public Group create(LocalDate logDate, LogRecord logRecord, GroupingParameters groupingParameters) {
        String username = null;
        if (groupingParameters.isGroupByUsername()) {
            username = logRecord.getUsername();
        }
        LocalDateTime dateTime = null;
        TemporalUnit temporalUnit = groupingParameters.getTimeUnitGroup();
        if (temporalUnit != null) {
            Duration unitDuration = temporalUnit.getDuration();
            if (unitDuration.compareTo(ChronoUnit.DAYS.getDuration()) <= 0) {
                LocalTime recordTime = logRecord.getTime();
                dateTime = LocalDateTime.of(logDate, recordTime);
                dateTime = dateTime.truncatedTo(temporalUnit);
            } else if (unitDuration.compareTo(ChronoUnit.MONTHS.getDuration()) <= 0) {
                dateTime = logDate.withDayOfMonth(1).atStartOfDay();
            } else {
                dateTime = logDate.withMonth(1).withDayOfMonth(1).atStartOfDay();
            }
        }
        return new Group(username, dateTime);
    }
}
