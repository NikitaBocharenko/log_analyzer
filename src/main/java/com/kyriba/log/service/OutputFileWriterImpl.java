package com.kyriba.log.service;

import com.kyriba.log.exception.OutputFileWritingException;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class OutputFileWriterImpl implements OutputFileWriter {
    private final Lock writeToOutputFileLock = new ReentrantLock();

    private static final Logger LOG = Logger.getLogger(OutputFileWriterImpl.class);

    @Override
    public void writeLinesToFile(List<String> lines, Path outputFilePath) {
        writeToOutputFileLock.lock();
        try {
            Files.write(outputFilePath, lines, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            LOG.error("Error while writing to output file: " + outputFilePath + "; " + e.getMessage(), e);
            throw new OutputFileWritingException("IOException: " + e.getMessage(), e);
        } finally {
            writeToOutputFileLock.unlock();
        }
    }

}
