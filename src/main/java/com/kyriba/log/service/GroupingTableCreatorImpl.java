package com.kyriba.log.service;

import com.kyriba.log.model.Group;
import com.kyriba.log.model.GroupingParameters;

import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class GroupingTableCreatorImpl implements GroupingTableCreator {
    private static final String DELIMITER = "------------------------------------------------------------";

    @Override
    public List<String> createTableRows(GroupingParameters parameters, Map<Group, Long> groupingResult) {
        List<String> result = new LinkedList<>();
        result.add("\n");
        result.add(DELIMITER);
        String tableCap = "";
        if (parameters.isGroupByUsername()) {
            tableCap += String.format("%20s", "Username");
        }
        if (parameters.getTimeUnitGroup() != null) {
            tableCap += String.format("%20s", parameters.getTimeUnitGroup());
        }
        tableCap += String.format("%20s", "Count of records");
        result.add(tableCap);
        result.add(DELIMITER);
        String dateTimePattern = getDateTimePattern(parameters.getTimeUnitGroup());
        groupingResult.forEach((group, aLong) -> {
            String tableRow = "";
            if (group.getUsername() != null) {
                tableRow += String.format("%20s", group.getUsername());
            }
            if (group.getDateTime() != null) {
                tableRow += String.format("%20s", group.getDateTime().format(DateTimeFormatter.ofPattern(dateTimePattern)));
            }
            tableRow += String.format("%20s", aLong);
            result.add(tableRow);
        });
        return result;
    }

    private String getDateTimePattern(TemporalUnit temporalUnit) {
        if (temporalUnit == null) {
            return "";
        }
        if (temporalUnit.getDuration().compareTo(ChronoUnit.SECONDS.getDuration()) <= 0) {
            return "yyyy/MM/dd HH:mm:ss";
        }
        if (temporalUnit.getDuration().compareTo(ChronoUnit.MINUTES.getDuration()) <= 0) {
            return "yyyy/MM/dd HH:mm";
        }
        if (temporalUnit.getDuration().compareTo(ChronoUnit.HOURS.getDuration()) <= 0) {
            return "yyyy/MM/dd HH";
        }
        if (temporalUnit.getDuration().compareTo(ChronoUnit.DAYS.getDuration()) <= 0) {
            return "yyyy/MM/dd";
        }
        if (temporalUnit.getDuration().compareTo(ChronoUnit.MONTHS.getDuration()) <= 0) {
            return "yyyy/MM";
        }
        return "yyyy";
    }
}
