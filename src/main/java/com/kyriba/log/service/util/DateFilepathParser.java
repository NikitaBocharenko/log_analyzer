package com.kyriba.log.service.util;

import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateFilepathParser {
    private static final String FILENAME_PATTERN = "ddMMyyyy";
    private static final String FILE_EXTENSION = ".log";

    public static LocalDate parse(Path filepath) {
        String filename = filepath.getFileName().toString();
        String logDateString = filename.substring(0, filename.indexOf("."));
        return LocalDate.parse(logDateString, DateTimeFormatter.ofPattern(FILENAME_PATTERN));
    }

    public static Path parse(Path directoryPath, LocalDate date) {
        String filename = date.format(DateTimeFormatter.ofPattern(FILENAME_PATTERN)) + FILE_EXTENSION;
        return directoryPath.resolve(filename);
    }
}
