package com.kyriba.log.service;

import com.kyriba.log.exception.LogAnalyzerThreadException;
import com.kyriba.log.exception.LogReadingException;
import com.kyriba.log.model.*;
import com.kyriba.log.service.util.DateFilepathParser;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogAnalyzerCallable implements Callable<Map<Group, Long>> {
    private final Set<Path> analyzedFiles;
    private final Map<Group, Long> groupingResult = new HashMap<>();
    private final FilterParameters filterParameters;
    private final GroupingParameters groupingParameters;
    private final OtherParameters otherParameters;
    private final LogRecordFilter logRecordFilter;
    private final GroupCreator groupCreator;
    private final OutputFileWriter outputFileWriter;

    private static final Logger LOG = Logger.getLogger(LogAnalyzerCallable.class);

    public LogAnalyzerCallable(AnalysisParameters parameters, Set<Path> analyzedFiles, LogRecordFilter logRecordFilter,
                               GroupCreator groupCreator, OutputFileWriter outputFileWriter) {
        this.analyzedFiles = analyzedFiles;
        this.filterParameters = parameters.getFilterParameters();
        this.groupingParameters = parameters.getGroupingParameters();
        this.otherParameters = parameters.getOtherParameters();
        this.logRecordFilter = logRecordFilter;
        this.groupCreator = groupCreator;
        this.outputFileWriter = outputFileWriter;
    }

    @Override
    public Map<Group, Long> call() {
        Path logFolderPath = otherParameters.getLogFolderPath();
        try (Stream<Path> walk = Files.walk(logFolderPath)) {
            walk
                    .filter(path -> Files.isRegularFile(path))
                    .filter(analyzedFiles::add)
                    .forEach(this::analyzeFile);
        } catch (IOException e) {
            LOG.error("IOException: " + e.getMessage(), e);
            throw new LogReadingException("IOException: " + e.getMessage(), e);
        }
        return groupingResult;
    }

    private void analyzeFile(Path filePath) {
        List<String> filteredRecords;
        LocalDate logCreationDate = DateFilepathParser.parse(filePath);
        try (Stream<String> fileLines = Files.lines(filePath)) {
            filteredRecords = fileLines
                    .map(LogRecord::new)
                    .filter(logRecord -> logRecordFilter.isRecordMatches(logRecord, filterParameters))
                    .peek(logRecord -> this.groupRecord(logRecord, logCreationDate))
                    .map(LogRecord::toString)
                    .collect(Collectors.toCollection(LinkedList::new));
        } catch (IOException e) {
            LOG.error("Error while reading file: " + filePath + "; " + e.getMessage(), e);
            return;
        }
        Path outputFilePath = otherParameters.getOutputFilePath();
        outputFileWriter.writeLinesToFile(filteredRecords, outputFilePath);
    }

    private void groupRecord(LogRecord logRecord, LocalDate logCreationDate) {
        Group group = groupCreator.create(logCreationDate, logRecord, groupingParameters);
        groupingResult.merge(group, 1L, Long::sum);
    }

}
