package com.kyriba.log.service;

import java.nio.file.Path;
import java.util.List;

public interface OutputFileWriter {
    void writeLinesToFile(List<String> filteredRecords, Path outputFilePath);
}
