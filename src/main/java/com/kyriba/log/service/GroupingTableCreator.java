package com.kyriba.log.service;

import com.kyriba.log.model.Group;
import com.kyriba.log.model.GroupingParameters;

import java.util.List;
import java.util.Map;

public interface GroupingTableCreator {
    List<String> createTableRows(GroupingParameters parameters, Map<Group, Long> groupingResult);
}
