package com.kyriba.log.service;

import com.kyriba.log.model.FilterParameters;
import com.kyriba.log.model.LogRecord;

import java.time.LocalTime;
import java.util.regex.Pattern;

public class LogRecordFilterImpl implements LogRecordFilter {

    @Override
    public boolean isRecordMatches(LogRecord record, FilterParameters parameters) {
        String parametersUsername = parameters.getUsername();
        if (parametersUsername != null && !parametersUsername.equals(record.getUsername())) {
            return false;
        }
        LocalTime recordTime = record.getTime();
        LocalTime timeFrom = parameters.getTimeFrom();
        if (timeFrom != null && timeFrom.isAfter(recordTime)) {
            return false;
        }
        LocalTime timeTo = parameters.getTimeTo();
        if (timeTo != null && timeTo.compareTo(recordTime) <= 0) {
            return false;
        }
        String messageRegex = parameters.getMessageRegex();
        return messageRegex == null || Pattern.matches(messageRegex, record.getMessage());
    }
}
